# Install

Install dependencies:

```
npm i
```

# Build

Build the bundle:

```
npm run build
```

Builder should be create bundle at folder `dist`. This folder should be mapping to the root url `/`.

# Development
## Run

```
npm start
```

## Compile

To create compiled swagger json from yml files run:

```
swagger-ui-watcher source --bundle=bundle
```

Where

* `source`: Path to source yml file
* `bundle`: Path to compiled json file

Example

```
swagger-ui-watcher ./inlearno/v1.yml --bundle=./swagger-compiled/inlearno.json
```