import SwaggerUI from 'swagger-ui'
import { SwaggerUIBundle, SwaggerUIStandalonePreset } from 'swagger-ui-dist';
import 'swagger-ui/dist/swagger-ui.css';

const ui = SwaggerUI({
  urls: [
    { url: 'api/inlearno/v1.yml', name: 'Inlearno Integration' }
  ],
  dom_id: '#swagger',
  defaultModelExpandDepth: 10,
  defaultModelsExpandDepth: 10,
  defaultModelRendering: "model",
  operationsSorter: 'method',
  filter: true,
  deepLinking: true,
  presets: [
    SwaggerUIBundle.presets.apis,
    SwaggerUIStandalonePreset
  ],
  plugins: [
    SwaggerUIBundle.plugins.DownloadUrl
  ],
  layout: "StandaloneLayout"
});